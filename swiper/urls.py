"""card_swiper URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from .views import CardSwipeView, CardListView, CardCreateView, MatchListView

app_name = 'swiper'
urlpatterns = [
    path('', CardListView.as_view(), name='card-list'),
    path('swipe/', CardSwipeView.as_view(), name='card-swipe'),
    path('list/', CardListView.as_view(), name='card-list'),
    path('matchlist/', MatchListView.as_view(), name='match-list'),
    path('create/', CardCreateView.as_view(), name='card-create'),
    #path('', network_list_view, name='network-list'),
	path('<int:pk>', CardSwipeView.as_view(), name='card-detail'),
    path('admin/', admin.site.urls),
]
