from django.db import models

from django.db.models.signals import pre_save

class CardState(models.IntegerChoices):
    YES = 1, 'Yes'
    NO = -1, 'No'
    UNSEEN = 0, 'Unseen'

# Create your models here.
class Card(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    detail_info = models.TextField(default=None, null=True, blank=True)
    image_src = models.TextField(default=None, null=True, blank=True)
    score = models.FloatField(default=1.0)
    prep_time = models.FloatField(default=0.0)
    state = models.SmallIntegerField(default=CardState.UNSEEN, choices=CardState.choices)
    img_path = models.TextField(default=None, null=True, blank=True)
    ingredients = models.CharField(max_length=100, null=False, blank=False)


class Match(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    score =  models.FloatField(default=0.0)
    detail_info = models.TextField(default=None, null=True, blank=True)
