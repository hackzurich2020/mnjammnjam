from django import forms

from .models import Card, CardState

class CardModelForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = [
            'name',
            'detail_info',
            'image_src',
            'score',
        ]