from django.urls import reverse

from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View, ListView, UpdateView, CreateView

from .models import Card, CardState, Match
from .forms import CardModelForm
from base.models import User, Ingredient
import ipdb

def next_card_to_show():
    print("Asking for next card to show")
    candidates = Card.objects.filter(state=CardState.UNSEEN).order_by('-score')
    print("Number of unseen cards: "+str(len(candidates)))
    if len(candidates) == 0:
        return None
    return candidates[0].id


def reset_all_swipes():
    print("reset all swipes")
    cards = Card.objects.all()
    for card in cards:
        card.state=CardState.UNSEEN
        card.save()

class CardObjectMixin(object):
    model = Card
    lookup = 'pk'

    def get_object(self):
        id_ = self.kwargs.get(self.lookup)
        if id_ == None:
            id_ = next_card_to_show()

        if id_ == None:
            return None
        else:
            return get_object_or_404(self.model, id=id_)

class CardSwipeView(CardObjectMixin, View):
    template_name = "card_swipe.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        print("get method")
        context = {}
        obj = self.get_object()
        context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None,  *args, **kwargs):
        # POST method
        print("post method")
        context = {}
        obj = self.get_object()
        if obj is not None:
            print(obj)
            print(request.POST)
            if 'submit-yes' in request.POST:
                print('submit YES')
                obj.state = CardState.YES
            elif 'submit-no' in request.POST:
                print('submit NO')
                obj.state = CardState.NO
            obj.save()

            if obj.state == CardState.YES:
                bob = User.objects.last()
                if obj.prep_time <= bob.cooking_pref:
                    ingr_in_cur_recp = []
                    ingrs = Ingredient.objects.all()
                    recipe_ingrs = obj.ingredients.split(" ")
                    for ingr in ingrs:
                        if ingr.name in recipe_ingrs:
                            ingr_in_cur_recp.append(ingr)
                    if len(ingr_in_cur_recp) > 3:
                        fat_cont = 0
                        for ingr in  ingr_in_cur_recp:
                            nutr = json.loads(ingr.nutritions)
                            if "total_fat" in nutr.keys():
                                fat_count += nutr["total_fat"]
                            elif "totalFat" in nutr.keys():
                                try:
                                    unit = nutr["totalFat"].split[" "]
                                except:
                                    unit =[0]
                                fat = float(unit[0])
                                fat_count += fat
                            else:
                                fat_count = 0.0
                        if bob.eat_goal == 0 and fat_count > 10:
                            match = Match(name = obj.name, score = 1.0, detail_info = obj.detail_info)
                            match.save()
                        elif bob.health_pref == 1:
                            if fat_count < 10:
                                match = Match(name = obj.name, score = 1.0, detail_info = obj.detail_info)
                                match.save()
                                
            print("request post: " + str(request.POST))
            context['object'] = obj
            return redirect(reverse('swiper:card-swipe'))

        else:
            if 'submit-reset' in request.POST:
                print('submit RESET')
                reset_all_swipes()
                return redirect(reverse('swiper:card-swipe'))

        return render(request, self.template_name, context)


class CardListView(ListView):
    template_name = 'card_list.html'
    queryset = Card.objects.all()

class MatchListView(ListView):
    template_name = 'match_list.html'
    queryset = Match.objects.all()



class CardCreateView(CreateView):
    template_name = 'card_create.html'
    form_class = CardModelForm
    queryset = Card.objects.all()

    def get_success_url(self):
        return reverse('swiper:card-list')
