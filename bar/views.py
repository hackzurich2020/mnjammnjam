
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib import messages
from django.contrib.messages import get_messages

from django.views.generic import View
from django.utils.decorators import method_decorator
from . import barCode

from base.models import Ingredient

from .models import AddableIngredient
import json
import requests
# Create your views here.
import ipdb

import base64


@method_decorator(csrf_exempt, name='dispatch')
class ScanView(View):
    template_name = "ScanItem.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        return render(request, self.template_name)

@method_decorator(csrf_exempt, name='dispatch')
class ScanBarCodeView(View):
    template_name = "ScanItem.html" # DetailView
    endpoint_url =  'https://eatfit-service.foodcoa.ch/products/{barcode_id}'

    # def get(self, request, id=None, *args, **kwargs):
    #     # GET method
    #     return render(request, self.template_name)


    def post(self, request, id=None,  *args, **kwargs):
        decodedData = barCode.decode(request.POST['imgBase64'])
        if decodedData:

            json_data = json.dumps(decodedData)
            #print(json_data)
            # TODO hard code we only use the first code found
            # 
            item_data = json.loads(json_data)[0]
      
            if item_data["type"] != 'QRCODE':
                result = {}
                url = self.endpoint_url.format(barcode_id=item_data["code"])
                response = requests.get(url , auth=(settings.CSS_BARCODE_APP_ID, settings.CSS_BARCODE_APP_KEY))

                if response.status_code == 200:  # SUCCESS
                    result = response.json()
                    #obj_result = json.loads(result)
                    prod_name = result['products'][0]['product_name_en']

                    prod_nutritions = []
                    for nutr in result['products'][0]['nutrients']:
                        prod_nutritions.append([nutr['name'], str(nutr['amount']) + " " + nutr['unit_of_measure']])
                    prod_image = result['products'][0]['image']
                   
                    result['success'] = True
                    json_nutritions = json.dumps(prod_nutritions, sort_keys=True)
                    ad_in = AddableIngredient(name = prod_name, nutritions = json_nutritions, image_src = prod_image)
                    ad_in.save()
                    return JsonResponse({"code" : 'next'})
                else:
                    result['success'] = False
                    if response.status_code == 404:  # NOT FOUND
                        result['message'] = 'No entry found for "%s"' % word
                    else:
                        result['message'] = 'The Barcode API is not available at the moment. Please try again later.'

            return JsonResponse(result,safe=False)

        return JsonResponse({"code" : 'NO BarCode Found'})


@method_decorator(csrf_exempt, name='dispatch')
class RecognizeView(View):
    api_key = "b1fab0da4af475bbe23771ac4e83e07e55501157"
    food_recognition_url = "https://api-beta.bite.ai/vision/"

    def post(self, request, id=None,  *args, **kwargs):
        decodedData = barCode.recognize(request.POST['imgBase64'])
        if decodedData:

            headers = {'Content-Type': 'application/json',
            'Authorization': 'Bearer {0}'.format(self.api_key)}

            img_base64 = base64.b64encode(decodedData.read())
            img_base64 = img_base64.decode("utf-8")
            
            data = {
                "base64": img_base64
            }
        
            with open('.tmp_img.jpg', "wb") as img:
            
                img.write(decodedData.read())

                
            response = requests.post(self.food_recognition_url, json=data, headers=headers)
            response = response.json()

            
            prod_nutritions = list(response['items'][0]['item']['nutrition_facts'][0]['nutrition'].items())
            prod_nutritions = json.dumps(prod_nutritions)
            prod_name = response['items'][0]['item']['name']
            prod_image = '.tmp_img.jpg'

            result = {}
                  
            result['success'] = True
            
            ad_in = AddableIngredient(name = prod_name, nutritions = prod_nutritions, image_src = prod_image)
            ad_in.save()
            return JsonResponse({"code" : 'next'})
            

    

class AddIngredientView(View):
    template_name = "bar/add_ingredient.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        print("get method")

        ad_in = AddableIngredient.objects.last()
        print(AddableIngredient.objects.all())
        context = {}
        prod_name = ad_in.name
        prod_image = ad_in.image_src
        prod_nutritions = json.loads(ad_in.nutritions)
     
        context = {'product_name': prod_name,
                   'image': prod_image,
                   'nutritions': prod_nutritions,
                   }
        return render(request, self.template_name, context)

    def post(self, request, id=None,  *args, **kwargs):
        # POST method
        pass
    


class IngredientAddedView(View):
    template_name = "bar/ingr_added.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method

        ad_in = AddableIngredient.objects.last()
        print(AddableIngredient.objects.all())
        ing = Ingredient(name = ad_in.name, img_path=ad_in.image_src, nutritions=ad_in.nutritions)
        ing.save()
        return render(request, self.template_name, {})

 
    
