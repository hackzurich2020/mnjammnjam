from django.db import models

# Create your models here.
class AddableIngredient(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    nutritions = models.CharField(max_length=1000, null=False, blank=False)
    image_src =models.CharField(max_length=1000, null=False, blank=False)
   
