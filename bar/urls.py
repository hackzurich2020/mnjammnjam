from django.conf.urls import url
from . import views
from django.urls import path
from django.views.generic import TemplateView

from .views import AddIngredientView, ScanBarCodeView, ScanView, RecognizeView, IngredientAddedView
app_name = 'bar'
urlpatterns = [
    path('', ScanView.as_view(), name='ScanItem'),
    path('decode/', ScanBarCodeView.as_view() ,name='decodeAjax'),
    path('recognize/', RecognizeView.as_view() ,name='recognizeAjax'),
    path('add_ingredient/',  AddIngredientView.as_view(), name="add_ingredient"),
    path('add_new_ingr/', IngredientAddedView.as_view(), name="add_new_ingr"),

]
