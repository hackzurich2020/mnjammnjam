from django.urls import reverse

from django.shortcuts import render, get_object_or_404

from django.views.generic import (
	CreateView,
	DetailView,
	ListView,
	UpdateView,
	DeleteView,
	View
	)

from .forms import RecipeModelForm, IngredientModelForm
from .models import Recipe, Ingredient, User
# Create your views here.


def home_view(request, *args, **kwargs):
	return render(request, "home.html", {})

class RecipeObjectMixin(object):
	model = Recipe
	lookup = 'pk'

	def get_object(self):
		id_ = self.kwargs.get(self.lookup)
		return get_object_or_404(self.model, id=id_)

class UserObjectMixin(object):
	model = User
	lookup = 'pk'

	def get_object(self):
		id_ = self.kwargs.get(self.lookup)
		return get_object_or_404(self.model, id=id_)


class RecipeListView(ListView):
	template_name = 'recipe/recipe_list.html'
	queryset = Recipe.objects.all()


class RecipeDetailView(RecipeObjectMixin, DetailView):
	template_name = 'recipe/recipe_detail.html'
	queryset = Recipe.objects.all()


class RecipeCreateView(CreateView):
	template_name = 'recipe/recipe_create.html'
	form_class = RecipeModelForm
	queryset = Recipe.objects.all()

	def get_success_url(self):
		return reverse('base:recipe-list')


class RecipeUpdateView(RecipeObjectMixin, UpdateView):
	template_name = 'recipe/recipe_create.html'
	form_class = RecipeModelForm
	queryset = Recipe.objects.all()


class RecipeDeleteView(RecipeObjectMixin, DeleteView):
	template_name = 'recipe/recipe_delete.html'

	def get_success_url(self):
		return reverse('base:recipe-list')



class IngredientObjectMixin(object):
	model = Ingredient
	lookup = 'pk'

	def get_object(self):
		id_ = self.kwargs.get(self.lookup)
		return get_object_or_404(self.model, id=id_)

class IngredientListView(ListView):
	template_name = 'ingredient/ingredient_list.html'
	queryset = Ingredient.objects.all()


class IngredientDetailView(IngredientObjectMixin, DetailView):
	template_name = 'ingredient/ingredient_detail.html'
	queryset = Ingredient.objects.all()


class IngredientCreateView(CreateView):
	template_name = 'ingredient/ingredient_create.html'
	form_class = IngredientModelForm
	queryset = Ingredient.objects.all()

	def get_success_url(self):
		return reverse('base:ingredient-list')


class IngredientUpdateView(IngredientObjectMixin, UpdateView):
	template_name = 'ingredient/ingredient_create.html'
	form_class = IngredientModelForm
	queryset = Ingredient.objects.all()


class IngredientDeleteView(IngredientObjectMixin, DeleteView):
	template_name = 'ingredient/ingredient_delete.html'

	def get_success_url(self):
		return reverse('base:ingredient-list')


class UserDetailView(UserObjectMixin, DetailView):
	template_name = 'user/user_detail.html'
	queryset = User.objects.all()
