from django import forms

from .models import Recipe, Ingredient

class RecipeModelForm(forms.ModelForm):
	class Meta:
		model = Recipe
		fields = [
			'name',
			'prep_steps',
			'prep_time',
		]

class IngredientModelForm(forms.ModelForm):
	class Meta:
		model = Ingredient
		fields = [
			'name',
		]