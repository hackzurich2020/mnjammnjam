from django.db import models
from django.urls import reverse

# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=200)
    prep_steps = models.TextField(default='Directions', null=True, blank=True)
    prep_time = models.PositiveSmallIntegerField() #preparation time in minutes
    img_path = models.TextField(default=None, null=True, blank=True)
    # image = models.ImageField(upload_to="static/images", height_field=None, width_field=None, max_length=100)

    def get_absolute_url(self):
        return reverse("base:recipe-detail", kwargs={"pk": self.pk})

#     def score_value(score_obj):
#         pass

class Ingredient(models.Model):
    name = models.CharField(max_length=200)
    recipes = models.ManyToManyField(Recipe)
    img_path = models.CharField(max_length=200)
    nutritions = models.CharField(max_length=1000)

    def get_absolute_url(self):
        return reverse("base:ingredient-detail", kwargs={"pk": self.pk})

    # def get_score_value(score_obj):
    #     return 42

# class ScoreMembership(models.Model):
#     group = models.ForeignKey(Recipe, on_delete=models.CASCADE)
#     user = models.ForeignKey(Ingredient, on_delete=models.CASCADE)
#     inviter = models.ForeignKey(
#         Ingredient,
#         on_delete=models.CASCADE,
#         related_name="score_membership_invites",
#     )
#
class EatingPref(models.IntegerChoices):
    ALL = 1, 'omnivore'
    VEGETARIAN = -1, 'vegetarian'
    VEGAN = 0, 'vegan'

class EatingGoal(models.IntegerChoices):
    SLIM = 1, 'slim'
    KEEP = -1, 'keep'
    GAIN = 0, 'gain'

class CookingPref(models.IntegerChoices):
    SIMPLE = 20, 'simple'
    MEDIOCRE = 40, 'mediocre'
    COMPLEs = 90, 'complex'

class Healthy(models.IntegerChoices):
    YES = 1, 'yes'
    NO = -1, 'no'

class User(models.Model):
    name = models.CharField(max_length=200)
    eat_pref = models.SmallIntegerField(default=EatingPref.ALL, choices=EatingPref.choices)
    eat_goal = models.SmallIntegerField(default=EatingGoal.KEEP, choices=EatingGoal.choices)
    cooking_pref = models.SmallIntegerField(default=CookingPref.MEDIOCRE, choices=CookingPref.choices)
    health_pref =  models.SmallIntegerField(default=Healthy.NO, choices=Healthy.choices)

class UserGroup(models.Model):
    name = models.CharField(max_length=200, default=None, blank=True)
    members = models.ManyToManyField(
        User,
        through='UserMembership',
        through_fields=('usergroup', 'user'),
    )

class UserMembership(models.Model):
    usergroup = models.ForeignKey(UserGroup, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    inviter = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="user_membership_invites",
    )

class Score(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(default=None, blank=True)
    min_score = models.FloatField()
    max_score = models.FloatField()
    default_score = models.FloatField()

class Fridge(models.Model):
    pass

class Profile(models.Model):
    pass
