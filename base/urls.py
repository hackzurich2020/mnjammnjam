from django.urls import path

from .views import (
    RecipeCreateView,
    RecipeDetailView,
    RecipeListView,
    RecipeUpdateView,
    RecipeDeleteView,

    IngredientCreateView,
    IngredientDetailView,
    IngredientListView,
    IngredientUpdateView,
    IngredientDeleteView,

    UserDetailView
    )

app_name = 'base'
urlpatterns = [
    path('recipe/list/', RecipeListView.as_view(), name='recipe-list'),
    path('recipe/create/', RecipeCreateView.as_view(), name='recipe-create'),
	path('recipe/<int:pk>', RecipeDetailView.as_view(), name='recipe-detail'),
    path('recipe/<int:pk>/update/', RecipeUpdateView.as_view(), name='recipe-update'),
    path('recipe/<int:pk>/delete/', RecipeDeleteView.as_view(), name='recipe-delete'),

    path('ingredient/list/', IngredientListView.as_view(), name='ingredient-list'),
    path('ingredient/create/', IngredientCreateView.as_view(), name='ingredient-create'),
	path('ingredient/<int:pk>', IngredientDetailView.as_view(), name='ingredient-detail'),
    path('ingredient/<int:pk>/update/', IngredientUpdateView.as_view(), name='ingredient-update'),
    path('ingredient/<int:pk>/delete/', IngredientDeleteView.as_view(), name='ingredient-delete'),
    path('user/<int:pk>', UserDetailView.as_view(), name='user-detail'),
]
