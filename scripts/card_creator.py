from base.models import Recipe
from swiper.models import Card

def clear_all_cards():
    cards = Card.objects.all()
    cards.delete()

def create_recipe_cards():
    recipes = Recipe.objects.all()
    for recipe in recipes:
        card = Card.objects.create()
        card.name = recipe.name
        card.detail_info = recipe.prep_steps
        card.score = recipe.prep_time
        card.img_path = recipe.img_path
        card.save()

def run():
    clear_all_cards()
    create_recipe_cards()