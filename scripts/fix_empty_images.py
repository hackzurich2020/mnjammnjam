from bs4 import BeautifulSoup
from requests_html import HTMLSession
import requests
import json

from base.models import Recipe, Ingredient
from . import card_creator

def get_google_img(query):
    """
    gets a link to the first google image search result
    :param query: search query string
    :result: url string to first result
    """
    url = "https://www.google.com/search?q=" + str(query) + "&source=lnms&tbm=isch"
    headers={'User-Agent':"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36"}

    try:
        html = requests.get(url, headers=headers).text
    except requests.ConnectionError:
        print("couldn't reach google")

    soup = BeautifulSoup(html, 'html.parser')
    print(html)
    image = soup.find("div",{"class":"rg_meta"})

    try:
        link =json.loads(image.text)["ou"]
    except AttributeError as error:
        print(error)
        print("couldn't find any images")
    except ValueError:
        print("ill formated json")

    return link

def fix_ingredient_images():
    ingredients = Ingredient.objects.all()
    for ingredient in ingredients:
        if ingredient.img_path == None:
            try:
                ingredient.img_path = get_google_img(ingredient.name)
                ingredient.save()
            except:
                pass

def fix_recipe_images():
    recipes = Recipe.objects.all()
    for recipe in recipes:
        if recipe.img_path == None:
            try:
                recipe.img_path = get_google_img(recipe.name)
                recipe.save()
            except:
                pass

def run():
    fix_ingredient_images()
    fix_recipe_images()
    card_creator.clear_all_cards()
    card_creator.create_recipe_cards()