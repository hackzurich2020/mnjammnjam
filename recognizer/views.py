from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
import base64
import requests

from django.http import JsonResponse

def index(request):
    data = {
        'name': 'Vitor',
        'location': 'Finland',
        'is_active': True,
        'count': 28
    }
    return JsonResponse(data)

def recognize_image(request):
    # FIXME
    img_path = "../mnjammnjam/placeholder.jpg"

    with open(img_path, "rb") as img:
        img_base64 = base64.b64encode(img.read())
    img_base64 = img_base64.decode("utf-8")

    api_key = "b1fab0da4af475bbe23771ac4e83e07e55501157"
    food_recognition_url = "https://api-beta.bite.ai/vision/"
    headers = {'Content-Type': 'application/json',
            'Authorization': 'Bearer {0}'.format(api_key)}

    data = {
        "base64": img_base64
    }

    response = requests.post(food_recognition_url, json=data, headers=headers)
    response = response.json()

    # print(response.keys())
    print(len(response['items']))
    print(">>>>name")
    print(response['items'][0]['item']['name'])
    print(">>>>ingredients")
    print(response['items'][0]['item']['ingredients'])
    print(">>>>nutrition_facts")
    print(response['items'][0]['item']['nutrition_facts'])
    return JsonResponse(response)
