from django.conf.urls import url
from django.contrib import admin
from recognizer import views

# TEMPLATE TAGGING
# app_name = 'recognizer' # needs to match in .html template

urlpatterns = [
    url(r'^$', views.recognize_image, name='recognize_image'),
    # url(r'^recognize_image/$', views.recognize_image, name='recognize_image'),
]