from django.urls import reverse

from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View, ListView, UpdateView, CreateView

from .models import Card, CardState
from .forms import CardModelForm


def next_card_to_show():
    print("Asking for next card to show")
    candidates = Card.objects.filter(state=CardState.UNSEEN)
    print("Number of unseen cards: "+str(len(candidates)))
    if len(candidates) == 0:
        return None
    return candidates[0].id


def reset_all_swipes():
    print("reset all swipes")
    cards = Card.objects.all()
    for card in cards:
        card.state=CardState.UNSEEN
        card.save()

class CardObjectMixin(object):
    model = Card
    lookup = 'pk'

    def get_object(self):
        id_ = self.kwargs.get(self.lookup)
        if id_ == None:
            id_ = next_card_to_show()

        if id_ == None:
            return None
        else:
            return get_object_or_404(self.model, id=id_)

class CardSwipeView(CardObjectMixin, View):
    template_name = "card_swipe.html" # DetailView

    def get(self, request, id=None, *args, **kwargs):
        # GET method
        print("get method")
        context = {}
        obj = self.get_object()
        context['object'] = obj
        return render(request, self.template_name, context)

    def post(self, request, id=None,  *args, **kwargs):
        # POST method
        print("post method")
        context = {}
        obj = self.get_object()
        if obj is not None:
            print(obj)
            print(request.POST)
            if 'submit-yes' in request.POST:
                print('submit YES')
                obj.state = CardState.YES
            elif 'submit-no' in request.POST:
                print('submit NO')
                obj.state = CardState.NO
            obj.save()

            print("request post: " + str(request.POST))
            context['object'] = obj
            return redirect(reverse('card-swipe'))

        else:
            if 'submit-reset' in request.POST:
                print('submit RESET')
                reset_all_swipes()
                return redirect(reverse('card-swipe'))

        return render(request, self.template_name, context)


class CardListView(ListView):
    template_name = 'card_list.html'
    queryset = Card.objects.all()


class CardCreateView(CreateView):
    template_name = 'card_create.html'
    form_class = CardModelForm
    queryset = Card.objects.all()

    def get_success_url(self):
        return reverse('card-list')